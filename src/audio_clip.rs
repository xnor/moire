use creek::{ReadDiskStream, ReadStreamOptions, SeekMode, SymphoniaDecoder};

use log::error;

/// AudioClip handles one (segment of an) audio file within a deck.
pub struct AudioClip {
    pub start_on_timeline_frames: f32,
    pub start_in_file_frames: f32,
    pub duration_frames: f32,
    pub sample_rate: usize,
    read_stream_main: ReadDiskStream<SymphoniaDecoder>,
    read_stream_headphones: ReadDiskStream<SymphoniaDecoder>,
}

impl AudioClip {
    pub fn new(
        file_path: &std::path::Path,
        start_on_timeline_seconds: f32,
        start_in_file_seconds: f32,
        end_in_file_seconds: Option<f32>,
    ) -> AudioClip {
        let opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let mut read_stream_main =
            ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, opts).unwrap();

        let opts = ReadStreamOptions {
            num_cache_blocks: 20,
            ..Default::default()
        };
        let mut read_stream_headphones =
            ReadDiskStream::<SymphoniaDecoder>::new(file_path, 0, opts).unwrap();

        let file_info = read_stream_main.info();

        let sample_rate = file_info.sample_rate.unwrap() as f32;

        let start_on_timeline_frames = start_on_timeline_seconds * sample_rate;
        let start_in_file_frames = start_in_file_seconds * sample_rate;
        let duration_frames = match end_in_file_seconds {
            Some(duration_seconds) => duration_seconds * sample_rate,
            None => file_info.num_frames as f32,
        };

        // Cache the start of the file into cache with index `0`.
        let _ = read_stream_main.cache(0, start_in_file_frames as usize);
        let _ = read_stream_headphones.cache(0, start_in_file_frames as usize);

        // Tell the stream to seek to the beginning of file. This will also alert the stream to the existence
        // of the cache with index `0`.
        read_stream_main
            .seek(start_in_file_frames as usize, Default::default())
            .unwrap();
        read_stream_headphones
            .seek(start_in_file_frames as usize, Default::default())
            .unwrap();

        // Wait until the buffer is filled before sending it to the process thread.
        read_stream_main.block_until_ready().unwrap();
        read_stream_headphones.block_until_ready().unwrap();

        AudioClip {
            start_on_timeline_frames,
            start_in_file_frames,
            duration_frames,
            sample_rate: sample_rate as usize,
            read_stream_main,
            read_stream_headphones,
        }
    }

    pub fn seek(&mut self, diff_seconds: f32) {
        let diff_frames = diff_seconds * self.sample_rate as f32;
        self.start_on_timeline_frames -= diff_frames;

        let main_playhead: usize = (self.read_stream_main.playhead() as isize
            + diff_frames as isize)
            .try_into()
            .unwrap_or(0);
        self.read_stream_main
            .seek(main_playhead, SeekMode::Auto)
            .unwrap();

        let headphones_playhead: usize = (self.read_stream_headphones.playhead() as isize
            + diff_frames as isize)
            .try_into()
            .unwrap_or(0);
        self.read_stream_headphones
            .seek(headphones_playhead, SeekMode::Auto)
            .unwrap();
    }

    pub fn process(
        &mut self,
        buffer_main: &mut [f32],
        first_frame_main: f32,
        buffer_headphones: &mut [f32],
        first_frame_headphones: f32,
        channels_per_frame: usize,
    ) -> bool {
        let main_processed = process_inner(
            self.start_on_timeline_frames,
            self.duration_frames,
            buffer_main,
            &mut self.read_stream_main,
            first_frame_main,
            channels_per_frame,
        );
        let headphones_processed = process_inner(
            self.start_on_timeline_frames,
            self.duration_frames,
            buffer_headphones,
            &mut self.read_stream_headphones,
            first_frame_headphones,
            channels_per_frame,
        );
        main_processed || headphones_processed
    }
}

fn process_inner(
    start_on_timeline_frames: f32,
    duration_frames: f32,
    buffer: &mut [f32],
    read_stream: &mut ReadDiskStream<SymphoniaDecoder>,
    first_frame: f32,
    channels_per_frame: usize,
) -> bool {
    if first_frame < start_on_timeline_frames
        || first_frame > start_on_timeline_frames + duration_frames
    {
        return false;
    }
    let channels = read_stream.info().num_channels;
    let frames = buffer.len() / channels as usize;
    match read_stream.read(frames) {
        Ok(read_data) => {
            if channels == 1 {
                let ch = read_data.read_channel(0);
                // At the end of the file, ch has fewer frames than buffer, so fill it with 0 at the end.
                let ch_iter = ch.iter().chain(std::iter::repeat(&0.0));
                for (output_frame, input_sample) in
                    buffer.chunks_mut(channels_per_frame).zip(ch_iter)
                {
                    for sample in output_frame {
                        *sample = *input_sample;
                    }
                }
            } else if channels == 2 {
                let ch1 = read_data.read_channel(0);
                let ch2 = read_data.read_channel(1);
                let stereo_frame_iter = ch1
                    .iter()
                    .zip(ch2)
                    // At the end of the file, ch1/2 have fewer frames than buffer,
                    // so fill with 0 at the end.
                    .chain(std::iter::repeat((&0.0, &0.0)));
                for (out_frame, in_frame) in
                    buffer.chunks_mut(channels_per_frame).zip(stereo_frame_iter)
                {
                    out_frame[0] = *in_frame.0;
                    out_frame[1] = *in_frame.1;
                }
            } else {
                panic!("Unhandled number of channels!");
            }
        }
        Err(e) => {
            match e {
                // This can happen when seeking near the end of the file
                creek::read::ReadError::EndOfFile => {}
                _ => error!("creek read error! {e:?}"),
            }
            for f in 0..frames {
                buffer[f] = 0f32;
                buffer[f + 1] = 0f32;
            }
        }
    }
    true
}
