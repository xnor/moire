use crate::audio_engine;
use std::sync::mpsc;

use log::{error, info, warn};

/// JackProcessHandler responds to JACK's requests to generate new buffers
/// of audio to pass to the audio interface (or another JACK client).
struct JackProcessHandler {
    audio_engine: audio_engine::AudioEngine,
    out_main_l: jack::Port<jack::AudioOut>,
    out_main_r: jack::Port<jack::AudioOut>,
    out_headphones_l: jack::Port<jack::AudioOut>,
    out_headphones_r: jack::Port<jack::AudioOut>,
}

impl JackProcessHandler {
    fn new(
        audio_engine: audio_engine::AudioEngine,
        out_main_l: jack::Port<jack::AudioOut>,
        out_main_r: jack::Port<jack::AudioOut>,
        out_headphones_l: jack::Port<jack::AudioOut>,
        out_headphones_r: jack::Port<jack::AudioOut>,
    ) -> JackProcessHandler {
        JackProcessHandler {
            audio_engine,
            out_main_l,
            out_main_r,
            out_headphones_l,
            out_headphones_r,
        }
    }
}

impl jack::ProcessHandler for JackProcessHandler {
    fn process(&mut self, _client: &jack::Client, ps: &jack::ProcessScope) -> jack::Control {
        assert_no_alloc::assert_no_alloc(|| {
            self.audio_engine.process();

            let out_main_l_buffer = self.out_main_l.as_mut_slice(ps);
            let out_main_r_buffer = self.out_main_r.as_mut_slice(ps);
            for (i, frame) in self
                .audio_engine
                .buffer_main_out
                .chunks(self.audio_engine.channels_per_frame)
                .enumerate()
            {
                out_main_l_buffer[i] = frame[0];
                out_main_r_buffer[i] = frame[1];
            }

            let headphones_l_buffer = self.out_headphones_l.as_mut_slice(ps);
            let headphones_r_buffer = self.out_headphones_r.as_mut_slice(ps);
            for (i, frame) in self
                .audio_engine
                .buffer_headphones_out
                .chunks(self.audio_engine.channels_per_frame)
                .enumerate()
            {
                headphones_l_buffer[i] = frame[0];
                headphones_r_buffer[i] = frame[1];
            }
        });
        jack::Control::Continue
    }

    fn buffer_size(&mut self, _: &jack::Client, buffer_size_frames: jack::Frames) -> jack::Control {
        info!("buffer size changed to {buffer_size_frames} frames");
        self.audio_engine
            .set_buffer_size(buffer_size_frames as usize);
        jack::Control::Continue
    }
}

pub enum JackNotification {
    SampleRateChanged(usize),
}

/// JackNotificationHandler receives notifications from JACK and sends them over
/// a channel to the appropriate targets that needs to respond to them.
struct JackNotificationHandler {
    tx: mpsc::SyncSender<JackNotification>,
}

impl JackNotificationHandler {
    pub fn new(tx: mpsc::SyncSender<JackNotification>) -> JackNotificationHandler {
        JackNotificationHandler { tx }
    }
}

impl jack::NotificationHandler for JackNotificationHandler {
    fn shutdown(&mut self, status: jack::ClientStatus, reason: &str) {
        info!("shutdown with status {status:?} because \"{reason}\"");
    }

    fn freewheel(&mut self, _: &jack::Client, is_enabled: bool) {
        info!(
            "freewheel mode {}",
            if is_enabled { "enabled" } else { "disabled" }
        );
    }

    fn sample_rate(&mut self, _: &jack::Client, sample_rate: jack::Frames) -> jack::Control {
        info!("sample rate changed to {sample_rate}");
        let _ = self
            .tx
            .send(JackNotification::SampleRateChanged(sample_rate as usize));
        jack::Control::Continue
    }

    fn xrun(&mut self, _: &jack::Client) -> jack::Control {
        info!("xrun occurred");
        jack::Control::Continue
    }
}

/// JackBackend initializes a new JACK client, creates and connects its ports,
/// and tells JACK to start processing.
pub struct JackBackend {
    _async_client: jack::AsyncClient<JackNotificationHandler, JackProcessHandler>,
}

fn check_jack_connection(result: Result<(), jack::Error>, port_number: usize) {
    if result.is_err() {
        error!("Error connecting to JACK output system:playback_{port_number}: {result:?}");
    }
}

impl JackBackend {
    pub fn new(mut audio_engine: audio_engine::AudioEngine) -> JackBackend {
        let (jack_client, _status) =
            jack::Client::new("moire", jack::ClientOptions::NO_START_SERVER).unwrap();

        let out_main_l = jack_client
            .register_port("main_L", jack::AudioOut::default())
            .unwrap();
        let out_main_r = jack_client
            .register_port("main_R", jack::AudioOut::default())
            .unwrap();
        let out_headphones_l = jack_client
            .register_port("headphones_L", jack::AudioOut::default())
            .unwrap();
        let out_headphones_r = jack_client
            .register_port("headphones_R", jack::AudioOut::default())
            .unwrap();

        let out_system_1 = jack_client.port_by_name("system:playback_1").unwrap();
        let out_system_2 = jack_client.port_by_name("system:playback_2").unwrap();

        let out_system_3 = jack_client.port_by_name("system:playback_3");
        let out_system_4 = jack_client.port_by_name("system:playback_4");
        if out_system_3.is_some() && out_system_4.is_some() {
            check_jack_connection(
                jack_client.connect_ports(&out_headphones_l, &out_system_3.unwrap()),
                3,
            );
            check_jack_connection(
                jack_client.connect_ports(&out_headphones_r, &out_system_4.unwrap()),
                4,
            );
        } else {
            warn!("JACK system outputs 3 & 4 not found; not connecting headphones output ports.");
        }

        check_jack_connection(jack_client.connect_ports(&out_main_l, &out_system_1), 1);
        check_jack_connection(jack_client.connect_ports(&out_main_r, &out_system_2), 2);

        audio_engine.set_buffer_size(jack_client.buffer_size() as usize);

        // This channel doesn't really have multiple producers; the only producer is
        // the JACK notification thread. However, rtrb::RingBuffer doesn't work in this
        // case because jack::Client::activate_async requires the NotificationHandler
        // to implement Sync, but rtrb::Producer does not implement Sync.
        let (notification_tx, notification_rx) = mpsc::sync_channel(64);
        audio_engine.set_jack_notification_rx(notification_rx);

        let process_handler = JackProcessHandler::new(
            audio_engine,
            out_main_l,
            out_main_r,
            out_headphones_l,
            out_headphones_r,
        );
        let notification_handler = JackNotificationHandler::new(notification_tx);

        JackBackend {
            _async_client: jack_client
                .activate_async(notification_handler, process_handler)
                .unwrap(),
        }
    }
}
