mod audio_clip;
mod audio_deck;
mod audio_engine;
mod gui;
mod jack_backend;
mod ramping_value;

mod waveform_analyzer;

#[global_allocator]
static A: assert_no_alloc::AllocDisabler = assert_no_alloc::AllocDisabler;

fn main() {
    pretty_env_logger::init();

    let (to_gui_tx, from_audio_rx) =
        rtrb::RingBuffer::<audio_engine::AudioEngineToGUIMessage>::new(2048);

    let (to_audio_tx, from_gui_rx) = rtrb::RingBuffer::<gui::GuiToAudioMessage>::new(2048);

    let audio_engine = audio_engine::AudioEngine::new(to_gui_tx, from_gui_rx);
    let _jack_backend = jack_backend::JackBackend::new(audio_engine);

    let mut garbage_collector = basedrop::Collector::new();
    let gui = gui::Gui::new(to_audio_tx, from_audio_rx, garbage_collector.handle());

    std::thread::spawn(move || loop {
        garbage_collector.collect();
        std::thread::sleep(std::time::Duration::from_secs(1));
    });

    gui.run();
}
