use crate::audio_clip::AudioClip;
use crate::audio_deck::AudioDeck;
use crate::audio_engine::AudioEngineToGUIMessage;
use crate::waveform_analyzer::{WaveformAnalysis, WaveformAnalyzer};
use basedrop::Owned;
use sixtyfps::{ComponentHandle, Model, ModelHandle, VecModel, Weak};
use std::cell::RefCell;
use std::rc::Rc;

mod generated {
    sixtyfps::include_modules!();
}

pub enum GuiToAudioMessage {
    AddDeck {
        deck: AudioDeck,
        vec: Owned<Vec<AudioDeck>>,
    },
    LoadClip {
        deck_index: usize,
        clip: AudioClip,
        vec: Owned<Vec<AudioClip>>,
    },
    VolumeChanged {
        deck_index: usize,
        value: f32,
    },
    HeadphonesChanged {
        deck_index: usize,
        value: bool,
    },
    ClipMoved {
        deck_index: usize,
        clip_index: usize,
        diff_seconds: f32,
    },
}

pub struct Gui {
    main_window: generated::MainWindow,
    _poll_audio_rx_timer: sixtyfps::Timer,
    _buffer_size_frames: Rc<RefCell<usize>>,
    _to_audio_tx: Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
}

/// Gui is responsible for running the GUI after the rest of the
/// application has been initialized.
impl Gui {
    pub fn new(
        to_audio_tx: rtrb::Producer<GuiToAudioMessage>,
        from_audio_rx: rtrb::Consumer<AudioEngineToGUIMessage>,
        garbage_collector: basedrop::Handle,
    ) -> Gui {
        let buffer_size_frames = Rc::new(RefCell::new(0));
        let to_audio_tx = Rc::new(RefCell::new(to_audio_tx));

        let main_window = generated::MainWindow::new();

        let main_window_weak = main_window.as_weak();

        main_window.on_volume_changed_on_deck(on_volume_changed_on_deck(&to_audio_tx));

        main_window.on_add_deck(on_add_deck(
            &main_window_weak,
            &buffer_size_frames,
            &to_audio_tx,
            &garbage_collector,
        ));

        main_window.on_load_file_to_deck(on_load_file_to_deck(
            &main_window_weak,
            &to_audio_tx,
            &garbage_collector,
        ));

        main_window.on_clip_seek_on_deck(on_clip_seek_on_deck(&to_audio_tx));

        main_window.on_headphones_changed_on_deck(on_headphones_changed_on_deck(&to_audio_tx));

        let poll_audio_rx_timer = sixtyfps::Timer::default();
        poll_audio_rx_timer.start(
            sixtyfps::TimerMode::Repeated,
            std::time::Duration::from_millis(100),
            on_gui_poll_timer(&main_window_weak, &buffer_size_frames, from_audio_rx),
        );

        Gui {
            main_window,
            _poll_audio_rx_timer: poll_audio_rx_timer,
            _buffer_size_frames: buffer_size_frames,
            _to_audio_tx: to_audio_tx,
        }
    }

    pub fn run(&self) {
        self.main_window.run();
    }
}

fn on_volume_changed_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
) -> impl FnMut(i32, f32) {
    let to_audio_tx = Rc::downgrade(&to_audio_tx);
    move |deck_index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(GuiToAudioMessage::VolumeChanged {
                deck_index: deck_index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_add_deck(
    main_window: &Weak<generated::MainWindow>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    to_audio_tx: &Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
    garbage_collector: &basedrop::Handle,
) -> impl FnMut() {
    let main_window = main_window.clone();
    let buffer_size_frames = Rc::downgrade(&buffer_size_frames);
    let to_audio_tx = Rc::downgrade(&to_audio_tx);
    let garbage_collector = garbage_collector.clone();
    move || {
        let main_window = main_window.upgrade().unwrap();
        let decks = main_window.get_decks();
        let deck_count;
        match decks
            .as_any()
            .downcast_ref::<VecModel<generated::DeckData>>()
        {
            None => {
                let model = VecModel::from(vec![empty_deckdata()]);
                main_window.set_decks(ModelHandle::new(Rc::new(model)));
                deck_count = 1;
            }
            Some(decks) => {
                decks.push(empty_deckdata());
                deck_count = decks.row_count();
            }
        }

        let mut audio_deck = AudioDeck::new();
        let buffer_size_frames = *buffer_size_frames.upgrade().unwrap().borrow();
        audio_deck.set_buffer_size(buffer_size_frames);

        let vec = Vec::<AudioDeck>::with_capacity(deck_count);

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(GuiToAudioMessage::AddDeck {
                deck: audio_deck,
                vec: Owned::new(&garbage_collector, vec),
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_load_file_to_deck(
    main_window: &Weak<generated::MainWindow>,
    to_audio_tx: &Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
    garbage_collector: &basedrop::Handle,
) -> impl FnMut(i32, f32) {
    let main_window = main_window.clone();
    let to_audio_tx = Rc::downgrade(&to_audio_tx);
    let garbage_collector = garbage_collector.clone();
    move |index, time| {
        let dialog_opened = std::time::Instant::now();

        // TODO: make async
        let path = match rfd::FileDialog::new().pick_file() {
            Some(picked) => picked,
            None => return,
        };

        let start_on_timeline_seconds = time + dialog_opened.elapsed().as_secs_f32();

        let main_window = main_window.upgrade().unwrap();
        let decks = main_window.get_decks();
        let decks = decks
            .as_any()
            .downcast_ref::<VecModel<generated::DeckData>>()
            .unwrap();
        let deck = decks.row_data(index as usize);

        let mut waveform_analyzer = WaveformAnalyzer::new(&path);
        waveform_analyzer.analyze();
        let waveform = draw_waveform(waveform_analyzer.result());

        let clips = deck
            .clips
            .as_any()
            .downcast_ref::<VecModel<generated::ClipData>>()
            .unwrap();
        clips.push(generated::ClipData {
            start_on_timeline_seconds,
            waveform,
        });

        let vec = Vec::with_capacity(clips.row_count());

        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(GuiToAudioMessage::LoadClip {
                deck_index: index as usize,
                clip: AudioClip::new(&path, start_on_timeline_seconds, 0.0, None),
                vec: Owned::new(&garbage_collector, vec),
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_clip_seek_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
) -> impl FnMut(i32, i32, f32) {
    let to_audio_tx = Rc::downgrade(&to_audio_tx);
    move |deck_index, clip_index, diff_seconds| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(GuiToAudioMessage::ClipMoved {
                deck_index: deck_index as usize,
                clip_index: clip_index as usize,
                diff_seconds,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_headphones_changed_on_deck(
    to_audio_tx: &Rc<RefCell<rtrb::Producer<GuiToAudioMessage>>>,
) -> impl FnMut(i32, bool) {
    let to_audio_tx = Rc::downgrade(&to_audio_tx);
    move |index, value| {
        let to_audio_tx = to_audio_tx.upgrade().unwrap();
        let mut to_audio_tx = to_audio_tx.borrow_mut();
        to_audio_tx
            .push(GuiToAudioMessage::HeadphonesChanged {
                deck_index: index as usize,
                value,
            })
            .expect("GUI to audio channel full!");
    }
}

fn on_gui_poll_timer(
    main_window: &Weak<generated::MainWindow>,
    buffer_size_frames: &Rc<RefCell<usize>>,
    mut from_audio_rx: rtrb::Consumer<AudioEngineToGUIMessage>,
) -> impl FnMut() {
    let main_window = main_window.clone();
    let buffer_size_frames = Rc::downgrade(&buffer_size_frames);
    move || {
        let mut last_received_time = 0f32;
        while from_audio_rx.slots() > 0 {
            match from_audio_rx.pop().unwrap() {
                AudioEngineToGUIMessage::ElapsedTime(t) => last_received_time = t,
                AudioEngineToGUIMessage::BufferSizeChanged(frames) => {
                    let buffer_size_frames = buffer_size_frames.upgrade().unwrap();
                    let mut buffer_size_frames = buffer_size_frames.borrow_mut();
                    *buffer_size_frames = frames;
                }
            }
        }
        let main_window = main_window.upgrade().unwrap();
        main_window.set_time(last_received_time);
        if last_received_time != 0f32 {
            let hours = (last_received_time / 3600f32).floor();
            let minutes = (last_received_time / 60f32).floor();
            let seconds = (last_received_time % 60f32).floor();
            let deciseconds =
                ((last_received_time - last_received_time.floor() as f32) * 10f32).floor();
            let string = format!("{}:{}:{}.{}", hours, minutes, seconds, deciseconds);
            main_window.set_duration_text(string.into());
        }
    }
}

fn empty_deckdata() -> generated::DeckData {
    generated::DeckData {
        clips: ModelHandle::new(Rc::new(VecModel::from(vec![]))),
    }
}

fn draw_waveform(analysis_data: &WaveformAnalysis) -> sixtyfps::Image {
    let height = 200;
    let mut waveform_buffer =
        sixtyfps::SharedPixelBuffer::<sixtyfps::Rgba8Pixel>::new(analysis_data.len(), height);
    let mut pixmap = tiny_skia::PixmapMut::from_bytes(
        waveform_buffer.make_mut_bytes(),
        analysis_data.len() as u32,
        height as u32,
    )
    .unwrap();
    pixmap.fill(tiny_skia::Color::WHITE);

    let path = {
        let mut path_builder = tiny_skia::PathBuilder::new();
        for (x, bin) in analysis_data.iter().enumerate() {
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.minimum * height as f32 / 2.0,
            );
            path_builder.line_to(
                x as f32,
                height as f32 / 2.0 + bin.maximum * height as f32 / 2.0,
            );
        }
        path_builder.finish().unwrap()
    };

    let mut paint = tiny_skia::Paint::default();
    paint.set_color_rgba8(0, 0, 255, 255);
    paint.anti_alias = true;

    pixmap.stroke_path(
        &path,
        &paint,
        &Default::default(),
        Default::default(),
        Default::default(),
    );

    sixtyfps::Image::from_rgba8_premultiplied(waveform_buffer)
}
