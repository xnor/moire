/// RampingValue is a wrapper for a continuous value whose changes
/// are linearly interpolated over each frame of an audio buffer to avoid
/// stair-step jumps in the value between cycles of the audio thread.
///
/// To use a RampingValue:
///   1. Initialize with RampingValue::new.
///   2. Set the number of steps that the RampingValue will iterate over
///      by setting the public `steps` member. In general this should be set
///      to the number of frames in an audio buffer.
///   3. When the value changes, set the public `value` member.
///   4. Use std::iter::zip with the RampingValue when iterating over the frames
///      of an audio buffer.
pub struct RampingValue {
    pub value: f32,
    old_value: f32,
    pub steps: usize,
}

impl RampingValue {
    pub fn new(initial_value: f32) -> Self {
        Self {
            value: initial_value,
            old_value: initial_value,
            steps: 0,
        }
    }
}

impl std::iter::IntoIterator for &mut RampingValue {
    type Item = f32;
    type IntoIter = RampingValueIterator;
    fn into_iter(self) -> Self::IntoIter {
        let iter = RampingValueIterator::new(self.value, self.old_value, self.steps);
        self.old_value = self.value;
        iter
    }
}

pub struct RampingValueIterator {
    old_value: f32,
    step_size: f32,
    steps: usize,
    step_index: usize,
}

impl RampingValueIterator {
    fn new(value: f32, old_value: f32, steps: usize) -> Self {
        RampingValueIterator {
            step_size: (value - old_value) / steps as f32,
            old_value,
            steps,
            step_index: 0,
        }
    }
}

impl std::iter::Iterator for RampingValueIterator {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.step_index == self.steps {
            return None;
        }
        let value = self.old_value + self.step_size * self.step_index as f32;
        self.step_index += 1;
        Some(value)
    }
}
