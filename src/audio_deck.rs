use crate::audio_clip::AudioClip;
use crate::ramping_value::RampingValue;
use basedrop::Owned;

/// AudioDeck handles one audio track on the timeline. An AudioDeck
/// can contain an arbitrary number of AudioClips.
pub struct AudioDeck {
    clips: Option<Owned<Vec<AudioClip>>>,
    pub buffer_main_out: Vec<f32>,
    pub buffer_headphones_out: Vec<f32>,
    channels_per_frame: usize,
    pub volume: RampingValue,
    pub headphones_enabled: RampingValue,
}

impl AudioDeck {
    pub fn new() -> AudioDeck {
        AudioDeck {
            clips: None,
            buffer_main_out: Vec::<f32>::new(),
            buffer_headphones_out: Vec::<f32>::new(),
            channels_per_frame: 2,
            volume: RampingValue::new(1.0),
            headphones_enabled: RampingValue::new(0.0),
        }
    }

    pub fn load_clip(&mut self, new_clip: AudioClip, mut new_vec: Owned<Vec<AudioClip>>) {
        // new_vec has a capacity enough for the old clips plus new_clip without
        // allocating on push, so move the old clips into new_vec.
        if let Some(clips) = &mut self.clips {
            for _ in 0..clips.len() {
                new_vec.push(clips.pop().unwrap());
            }
        }
        new_vec.push(new_clip);
        new_vec.sort_unstable_by(|a, b| {
            a.start_on_timeline_frames
                .partial_cmp(&b.start_on_timeline_frames)
                .unwrap()
        });
        self.clips = Some(new_vec);
    }

    pub fn seek_clip(&mut self, clip_index: usize, diff_seconds: f32) {
        self.clips.as_mut().unwrap()[clip_index].seek(diff_seconds);
    }

    pub fn process(&mut self, playhead_main_frames: f32, headphones_playhead_frames: f32) {
        let mut any_clips_processed = false;
        if let Some(clips) = &mut self.clips {
            for clip in clips.iter_mut() {
                let clip_processed = clip.process(
                    &mut self.buffer_main_out,
                    playhead_main_frames,
                    &mut self.buffer_headphones_out,
                    headphones_playhead_frames,
                    self.channels_per_frame,
                );
                if !any_clips_processed && clip_processed {
                    any_clips_processed = true;
                }
            }
        }
        if !any_clips_processed {
            for sample in self.buffer_main_out.iter_mut() {
                *sample = 0f32;
            }
            for sample in self.buffer_headphones_out.iter_mut() {
                *sample = 0f32;
            }
        }

        ramp_gain_interleaved(
            &mut self.buffer_headphones_out,
            self.channels_per_frame,
            &mut self.headphones_enabled,
        );

        ramp_gain_interleaved(
            &mut self.buffer_main_out,
            self.channels_per_frame,
            &mut self.volume,
        );
    }

    pub fn set_buffer_size(&mut self, buffer_size_frames: usize) {
        self.buffer_main_out
            .resize(buffer_size_frames * self.channels_per_frame, 0f32);
        self.buffer_headphones_out
            .resize(buffer_size_frames * self.channels_per_frame, 0f32);
        self.volume.steps = buffer_size_frames;
        self.headphones_enabled.steps = buffer_size_frames;
    }
}

fn ramp_gain_interleaved(buffer: &mut [f32], channels_per_frame: usize, gain: &mut RampingValue) {
    for (frame, gain) in buffer.chunks_mut(channels_per_frame).zip(gain) {
        for sample in frame {
            *sample *= gain;
        }
    }
}
